import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

class FenListe extends Stage
{
    private ListView<String> liste1 = new ListView<>();
    private ListView<String> liste2 = new ListView<>();

    private Button leftToRight = new Button(">");
    private Button rightToLeft = new Button("<");

    FenListe()
    {
        this.setTitle("Drag and Drop - TP 5");
        Scene scene = new Scene(init());

        liste1.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        for(int i = 1; i <= 10; ++i)
            liste1.getItems().add("Element " + i);

        this.setScene(scene);
        this.sizeToScene();
        this.setResizable(false);
    }

    private Parent init()
    {
        HBox root = new HBox();
        VBox buttons = new VBox();

        root.setSpacing(10);

        buttons.setAlignment(Pos.CENTER);
        buttons.setSpacing(20);

        buttons.getChildren().addAll(leftToRight, rightToLeft);

        root.getChildren().addAll(liste1, buttons, liste2);

        leftToRight.setOnAction(e -> transfet(e));
        rightToLeft.setOnAction(e -> transfet(e));

        liste1.setOnDragDetected(e -> startDrag());

        liste2.setOnDragOver(e -> e.acceptTransferModes(TransferMode.COPY_OR_MOVE));
        liste2.setOnDragDropped(e -> drop(e));

        liste1.setOnDragDone(e -> dragDone(e));


        return root;
    }

    private void transfet(ActionEvent e)
    {
        if(e.getSource() == leftToRight)
        {
            String elem = liste1.getSelectionModel().getSelectedItem();

            if(elem != null)
            {
                liste2.getItems().add(elem);
                liste1.getItems().remove(elem);
            }
        }
        else if(e.getSource() == rightToLeft)
        {
            String elem = liste2.getSelectionModel().getSelectedItem();

            if(elem != null)
            {
                liste1.getItems().add(elem);
                liste2.getItems().remove(elem);
            }
        }
    }

    private void startDrag()
    {
        Dragboard db = liste1.startDragAndDrop(TransferMode.COPY_OR_MOVE);

        ClipboardContent content = new ClipboardContent();

        content.putString(liste1.getSelectionModel().getSelectedItem());

        db.setContent(content);
    }

    private void drop(DragEvent e)
    {
        Dragboard db = e.getDragboard();

        liste2.getItems().add(db.getString());

        e.setDropCompleted(true);
    }

    private void dragDone(DragEvent e)
    {
        if(e.getTransferMode() == TransferMode.MOVE)
            liste1.getItems().remove(liste1.getSelectionModel().getSelectedIndex());
    }
}
